QUICK NOTE:
-----------
**you MUST have:**
-  `strdup()`, `strncasecmp()`, `strcasecmp()`, `putenv()`, `vsnprintf()`, `snprintf()`
   installed on your system to get `ecgilib` to compile
- `gcc --shared` fails on some systems - ignore this and perform a simple
  `make install` - you should end up with a `libecgi.a` then
- some systems need `gcc -G` instead - changed SHAREDOPT in `Makefile` to `-G`

INSTALL
-------
just perform a "make" and "make install" within the ecgi dir. If you
don't want `ecgilib` installed to `/usr/bin`, `/usr/include`, `/usr/lib`.
Change the Makefile accordingly.

HELP
----
If anyone has experience with autoconf and wants to help me: great!
Especially interesting would be the part, that prevent the functions
mentioned above from erroring with specific compilers.

About ecgi
=
ecgi (easy CGI Libary) is an ANSI C library for the
creation of CGI-based Web applications. It transparently
supports the `CGI` methods `GET` and `POST`, and also
`multipart/form-data` file uploads. The user interface is
designed to be as easy as possible and maintains full
compatibility to cgic 0.5.

It also contains a library independent introduction to
`CGI` programming with `C`, an .html to .h HTML template
preprocessor, and fast, block-allocating memory files.

DOCS
=
The documentation is comprised of ecgitut.* files.

The original documentation was made in Word docs -- UGH.
As a result, attempts to convert them to HTML format
resulted in FAR less than desirable output. So the next
version of ecgi promises, among other things, *properly*
formatted HTML documentation. However, all the ACII text
documents and the ecgitut. files are all in good shape.
OTOH If you *do* use Word, or have a RTF reader. You'll
feel right at home opening ecgitut.doc.

I will add a pdf later, but no time for this ATM.

INSTALLATION
=
See [INSTALL](INSTALL.md) for build and installation.
